**HTML canvas and p5.js beta test course**

The order of the readings and assignments is as follows:

1. [lessons/Canvas Lesson One](lessons/Canvas_Lesson_One.md)
2. [lessons/Canvas Lesson OneA](lessons/Canvas_Lesson_OneA.md)
3. [lessons/Canvas Lesson Two](lessons/Canvas_Lesson_Two-P5.md)
4. [lessons/Canvas Lesson Two](lessons/Canvas_Lesson_Two-P5.md)
5. [lessons/Canvas Lesson Three](lessons/Canvas_Lesson_Three-OOP_and_p5.md)
6. [lessons/exercises/Exercise 2](lessons/exercises/Exercise_2-Debug_Drag_and_Drop.md)
7. [lessons/Canvas Lesson Four](lessons/Canvas_Lesson_Four-Additional_Resources.md)
8. [lessons/exercises/Exercise 3 (optional)](lessons/Canvas_Lesson_Four-Additional_Resources.md)