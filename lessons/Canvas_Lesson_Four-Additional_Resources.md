Now that we have gotten a small preview of how p5 can streamline the process of creating html canvas, let's talk for a moment about some of the functionality p5 provides developers.

Remember that the act of creating visual elements is an expression of an internal vision. Start with a vision of what you want the canvas to look like, and then see if there is anything p5 can offer that can help you create this vision. Even if there is nothing that can directly accomplish the goal you have in mind, think about how you can leverage some of this functionality in the internal programming logic.


**Physics simulation and interactivity**

Suppose you wished to use a canvas to create an interactive element where the user could have objects collide with one another and move realistically.

The [vector](https://p5js.org/reference/#/p5.Vector) and other associated [math functions](https://p5js.org/reference/#group-Math) can help make this process much faster. Do note that an understanding of physics and vector math may be necessary to make more and more realistic simulations.

**Sound input and visualization**

p5 has the ability to use sounds as part of your canvas (for example, in order to create music visualizers), p5 has a [sound library](https://p5js.org/reference/#/libraries/p5.sound) to help you accomplish just that. 

**More complex user interfaces**

If your vision includes a more typical html interface involving buttons, sliders and the like, then p5 has a [DOM library](https://p5js.org/reference/#/libraries/p5.dom). This library helps developers create ways for users to interact ["beyond the canvas"](https://github.com/processing/p5.js/wiki/Beyond-the-canvas), in their own words. 

**Understanding documentation and examples**

The p5 site has an extremely extensive and interactive [examples](https://p5js.org/examples/) page where different functions and effects are demonstrated with code.

In particular, if you are interested in simulation aspects or drawing specific shapes, the examples page is the fastest and easiest way to get your hands on reproducible, working code.

