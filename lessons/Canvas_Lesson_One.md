**The HTML Canvas**

So far, we have covered (in activities like the Pacman Game) how to use HTML elements as visual representations of game objects.

However, HTML has the capability to do more than just that. 

The HTML \<canvas>  element has several special properties and methods which make it simple to create visual elements within an html page without having to create and re-render html elements constantly.

For the most part, this is handled by Javascript.

Let's walk through a quick example.

``` html
<canvas id="my-canvas"></canvas>
```

This is a canvas element, with a given id of "my_canvas". We can select this canvas using `document.getElementById`. We also want to modify the context of this element. Consider the context to be the actual place the visual elements are drawn to. In a `<script>` tag at the end of the html file, add the following code.

```
let canvasElement = document.getElementById("my-canvas");
let canvasContext = canvasElement.getContext("2d");
```

Make sure that your code up to this point works with no errors in the developer console, and then write the following code to create your first drawing on this canvas!


```
canvasContext.lineWidth = 10;
canvasContext.strokeStyle = 'blue';
canvasContext.strokeRect(10, 10, 70, 70);
canvasContext.fillStyle = 'red';
canvasContext.fillRect(50, 30, 40, 40);

```
Your html page should look like the following:

![](./images/lesson_one.png)


Take a moment and think about how you would go about making the same visuals using `<div>`s and css. 


The canvas context has several properties and methods to draw shapes and control the color of each shape. `strokeStyle` and `fillStyle` determine the look of the shapes created by the `strokeRect` and `fillRect` methods, respectively. 

There are many more methods we have access to using the canvasContext object, feel free to take a few minutes to tinker with the code and explore the Mozilla Developer Network documentation for canvas and see what you can do!

