Now that we have a basic understanding of how to use p5, let's talk about how to use Object Oriented Programming in order to easily create more dynamic behavior for visual elements in p5.

Set up a new html file that uses p5, and in the javascript file write the following:

```
var gravityAcceleration = 1;
var ball1;
var ballArray = [];
function setup(){
    createCanvas(720, 480);
    ball1 = new Ball(width/2, 0);
}

function draw() {
    background(25);
    ball1.move();
    ball1.display();
}

function Ball(xPosition, yPosition){
    this.x = xPosition;
    this.y = yPosition;
    this.diameter = 50;
    this.speed = 0;
    this.move = function(){
        this.y += this.speed;
        if (this.y >= (height-this.diameter/2)) {
            this.y = height - this.diameter / 2;
            this.speed = -0.9 * this.speed;
        }else{
            this.speed += gravityAcceleration;
        }
    }
    this.display = function(){
        ellipse(this.x, this.y, this.diameter, this.diameter);
    }
}
```

If everything is working, the browser should show a white ball bouncing on a black background!  Let's take a closer look at the Ball constructor function and how it is used in the script.

The `move` method uses some math to simulate how a ball drops in code. This is not a perfect simulation, as you may notice the ball bounces perpetually and never completely comes to rest.

The `display` method simply calls the `ellipse` function built into p5 and passes the x and y properties of the object into the first two parameters.

Now, where do we use these methods? Are they invoked automatically like `draw` and `setup`? No, we must invoke them manually within the `draw` function. First, we must update the variables and object properties within the `move` function in order to change how the canvas looks from one frame to another. Next, we invoke `display` in order to tell the canvas to redraw the ellipse representing the ball. 

