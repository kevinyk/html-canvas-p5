**Canvas Robot**

Using html canvas and the methods we have learned so far *only*, recreate the robot drawn below:

![](./images/robot.png)

To help get you started, use the provided html code. It will create a basic canvas with a grey rectangle. If you are stuck on how to start, try to create a few basic rectangles and "pixel push" them into the correct size and position.

```
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Build a Robot</title>
</head>
<body>
    <canvas id="my_canvas"></canvas>
</body>
<script>
    let canvasElement = document.getElementById('my_canvas');
    let canvasContext = canvasElement.getContext('2d');
    canvasContext.fillStyle = 'grey';
    canvasContext.fillRect(30, 10, 100, 125);
</script>
</body>
</html
```