**Debug Drag and Drop**

You are tasked with debugging existing code using p5. The canvas is supposed to create a ball that follows your current mouse position and lets go of the ball when you click the mouse. 

However, the ball does not drop from the mouse's current position. Instead, it floats in the air.

The code for both files is below. 

Hint: You don't need to change any of the code inside of the class Ball. 

index.html:

```
<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/p5.js"></script>
    <script src="sketch.js"></script>
</head>
<body>
    
</body>
</html>
```


sketch.js:

```
var gravityAcceleration = 1;
var ballArray = [];
function setup() {
    createCanvas(720, 480);
}

function draw() {
    background(25);
    for(var i = 0; i< ballArray.length; i++){
        ballArray[i].display();
    }
}
function mouseReleased(){
    ballArray.push(new Ball(width/2, 0));
}
function Ball(xPosition, yPosition) {
    this.x = xPosition;
    this.y = yPosition;
    this.diameter = 50;
    this.speed = 0;
    this.move = function () {
        this.y += this.speed;
        if (this.y >= (height - this.diameter / 2)) {
            this.y = height - this.diameter / 2;
            this.speed = -0.9 * this.speed;
        } else {
            this.speed += gravityAcceleration;
        }
    }
    this.display = function () {
        ellipse(this.x, this.y, this.diameter, this.diameter);
    }
}
```
