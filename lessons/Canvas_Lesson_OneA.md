**Understanding the Canvas**

Let's take a moment to dive a little deeper into how the HTML Canvas is set up.

The HTML Canvas operates on a two-dimensional grid (x,y). The horizontal axis (the x-axis) and the vertical axis (the y-axis) are measured in pixels from the left and top, respectively.

This means that the canvas needs shapes and lines to have coordinates that follow a system where the top-left corner is (0,0).

In addition, each shape or line drawn in the canvas must have certain properties defined in order to have any visual appearance. In the previous example, the `linewidth`, `strokeStyle`, and `fillStyle` properties on the canvas must be defined before successfully using the `strokeRect` and `fillRect` methods.

Let's take a look at the code again here for reference.

```
canvasContext.lineWidth = 10;
canvasContext.strokeStyle = 'blue';
canvasContext.strokeRect(30, 10, 70, 70);
canvasContext.fillStyle = 'red';
canvasContext.fillRect(50, 30, 40, 40);
```

In technical terms, think of this as similar to for most HTML elements, the border property exists but has values that make it invisible.

**The Canvas Analogy**

In a more intuitive sense, think of trying to draw on the HTML canvas as similar to the act of painting on an actual canvas. Before placing a brush on the canvas, an artist must choose a color and thickness of brush with which to paint their art. The `linewidth` is the thickness of the brush. The `strokeStyle` and `fillStyle` define the colors of paint.

Then, imagine the `strokeRect` and `fillRect` methods as the act of physically putting the brush on the canvas. This might seem obvious, but it is an important comparison to make.

Before drawing a new shape on the HTML canvas, think about if any of these properties need to change between the current configuration and the new one. Going back to the analogy, think about if the artist drawing the picture needs to change the color of paint on their brush or their brush itself before moving on. 

In short, defining properties of the canvas context => changing the paint and brush. Invoking methods to create shapes => the act of painting. This analogy will still be helpful once we get into using p5.js.